var async = require('async');
var _ = require('underscore');
var ObjectID = require('mongodb').ObjectID;
var http = require('http');
var querystring = require("querystring");
var moment = require("moment");

var utils = require("../includes/utils.js");




// =======================================================================================
                              // product_list_shop
// =======================================================================================

exports.product_list_shop = function(req, res){
  console.log("product_list_shop");

  var id = req.body.id;
  var filter1 = {'_id':ObjectID(id)};
  var result = [];

  async.waterfall([
    function(callback) {
      req.db.collection('').find(filter1).sort({}).toArray(callback);
    },
    function(data,callback) {
      if (!data){
        callback({"http_code":"404","message":"No Shop List Found", "dump":{}},{});
        return;
      }
      result = data;
      callback(null,{});
    }
  ],
  function(err,data) {
    if (err) {
      console.error(err);
      var api_err = {"http_code":"500","message":"system error", "dump":err};
      res.send(500,api_err);
    } else {
      res.send(200,result);
    }
  });

};



// =======================================================================================
                              // shopping_cart_getone
// =======================================================================================

exports.shopping_cart_getone = function(req, res){
  console.log("shopping_cart_getone");
  var id = req.body.id;

  var result = {};

  async.waterfall([
    function(callback) {
      var query = {'_id':ObjectID(id)};
      req.db.collection('').find(query).toArray(callback);
    },
    function(data,callback) {
      if (data.length < 1) {
        callback({"http_code":"404","message":"Shopping Cart not found", "dump":{}});
        return;
      }
      result = data[0];
      callback(null,{});
    }
  ],
  function(err,data) {
    if (err) {
      if (err.http_code) {
        res.send(err.http_code,err);
      } else {
        var api_err = {"http_code":"500","message":"system error", "dump":err};
        res.send(500,api_err);
      }
    } else {
      res.send(200,result);
    }
  });
};





