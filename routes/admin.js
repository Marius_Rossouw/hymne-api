var async = require('async');
var _ = require('underscore');
var ObjectID = require('mongodb').ObjectID;
var http = require('http');
var querystring = require("querystring");
var moment = require("moment");

var utils = require("../includes/utils.js");




// =======================================================================================
                              // merchant_list
// =======================================================================================

exports.merchant_list = function(req, res){
  console.log("merchant_list");
  var result = {};

  async.waterfall([
        function(callback) {

            req.db.collection('').find().sort({}).toArray(callback);
        },
        function(data,callback) {
          console.log(JSON.stringify(data));
          callback(null,{});
        }
      ],
      function(err,data) {
        utils.gen_result(res,err,200,result);
      });
};




// =======================================================================================
                              // merchant_update
// =======================================================================================

exports.merchant_update = function(req, res){
  console.log("merchant_update");
  var result = {};

  var id = req.body.id;
  var query = {'_id':ObjectID(id)};
  var options = {"upsert":false, "multi":false};

  var o = req.body;
  // delete o._id;
  var u = {$set:o};

  async.waterfall([
        function(callback) {
          req.db.collection('').update(query,u,options,function(err){
            callback(err,{});
          });
        },
      ],
      function(err,data) {
        utils.gen_result(res,err,200,result);
      });
};