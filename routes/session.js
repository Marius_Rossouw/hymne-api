var async = require('async');
var _ = require('underscore');
var ObjectID = require('mongodb').ObjectID;
var http = require('http');
var querystring = require("querystring");
var moment = require("moment");

var utils = require("../includes/utils.js");

exports.login = function(req, res) {
  //console.log(req.params);
  //console.log(req.query);
  //console.log(req.body);

  var result = {};
  async.waterfall([
        function(callback) {
          var projection = {password:false };
          var filter = {'email':req.body.auth_identifier,'password':req.body.auth_password};
          req.db.collection('persons').find(filter,projection).toArray(callback);
        },
        function(data,callback) {
          if (!data || data.length === 0) {
            callback({"http_code":"404","message":"incorrect credentials1"},{});
          } else {
            callback(null,data[0]);
          }
        },
        function(data,callback) {
          if (data._id) {
              result.token = data._id;
              result.profile_id = data._id;
              result.first_name = data.firstname;
              result.last_name = data.lastname;
              result.profile_name = utils.concat_names([data.firstname,data.lastname,data.company_name]);
              result.email = data.email;
              result.role_merchant = data.role_merchant;
              result.role_admin = data.role_admin;
              callback(null,{});
          } else {
            callback({"http_code":"404","message":"incorrect credentials2"},{});
          }
        }
      ],
      function(err,data) {
        utils.gen_result(res,err,200,result);
      });
};
