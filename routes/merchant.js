var async = require('async');
var _ = require('underscore');
var ObjectID = require('mongodb').ObjectID;
var http = require('http');
var querystring = require("querystring");
var moment = require("moment");

var utils = require("../includes/utils.js");


// =======================================================================================
                              // terms_update
// =======================================================================================

exports.terms_update = function(req, res){
  console.log("terms_update");

  var id = req.body.id;
  var query = {'_id':ObjectID(id)};
  var options = {"upsert":false, "multi":false};

  var o = req.body;
  // delete o._id;
  var u = {$set:o};

  var resultdata = {};
  async.waterfall([
        function(callback) {
          req.db.collection('').update(query,u,options,function(err){
            callback(err,{});
          });
        },
      ],
      function(err,data) {
        if (err) {
          console.error(err);
          if (err.http_code) {
            res.send(err.http_code,err);
          } else {
            var api_err = {"http_code":"500","message":"system.error", "dump":err};
            res.send(500,api_err);
          }
        } else {
          res.send(200,resultdata);
        }
      });
};




// =======================================================================================
                              // paypal_update
// =======================================================================================

exports.paypal_update = function(req, res){
  console.log("paypal_update");

  var id = req.body.id;
  var query = {'_id':ObjectID(id)};
  var options = {"upsert":false, "multi":false};

  var o = req.body;
  // delete o._id;
  var u = {$set:o};

  var resultdata = {};
  async.waterfall([
        function(callback) {
          req.db.collection('').update(query,u,options,function(err){
            callback(err,{});
          });
        },
      ],
      function(err,data) {
        if (err) {
          console.error(err);
          if (err.http_code) {
            res.send(err.http_code,err);
          } else {
            var api_err = {"http_code":"500","message":"system.error", "dump":err};
            res.send(500,api_err);
          }
        } else {
          res.send(200,resultdata);
        }
      });
};




