var async = require('async');
var _ = require('underscore');
var ObjectID = require('mongodb').ObjectID;
var http = require('http');
var querystring = require("querystring");
var moment = require("moment");

var utils = require("../includes/utils.js");




// =======================================================================================
                              // pay_test
// =======================================================================================

exports.pay_test = function(req, res){
  console.log("pay_test");

  var id = req.body.id;
  var query = {'_id':ObjectID(id)};
  var options = {"upsert":false, "multi":false};

  var o = req.body;
  // delete o._id;
  var u = {$set:o};

  var resultdata = {};
  async.waterfall([
        function(callback) {
          req.db.collection('').update(query,u,options,function(err){
            callback(err,{});
          });
        },
      ],
      function(err,data) {
        if (err) {
          console.error(err);
          if (err.http_code) {
            res.send(err.http_code,err);
          } else {
            var api_err = {"http_code":"500","message":"system.error", "dump":err};
            res.send(500,api_err);
          }
        } else {
          res.send(200,resultdata);
        }
      });
};




// =======================================================================================
                              // purchase_history_list
// =======================================================================================

exports.purchase_history_list = function(req, res){
  console.log("purchase_history_list");

  var id = req.body.id;
  var filter1 = {'_id':ObjectID(id)};
  var result = [];

  async.waterfall([
    function(callback) {
      req.db.collection('').find(filter1).sort({}).toArray(callback);
    },
    function(data,callback) {
      if (!data){
        callback({"http_code":"404","message":"No Purchase History", "dump":{}},{});
        return;
      }
      result = data;
      callback(null,{});
    }
  ],
  function(err,data) {
    if (err) {
      console.error(err);
      var api_err = {"http_code":"500","message":"system error", "dump":err};
      res.send(500,api_err);
    } else {
      res.send(200,result);
    }
  });

};



