var async = require('async');
var _ = require('underscore');
var ObjectID = require('mongodb').ObjectID;
var http = require('http');
var querystring = require("querystring");
var moment = require("moment");

var utils = require("../includes/utils.js");




// =======================================================================================
                              // register_init
// =======================================================================================

exports.register_init = function(req, res){
  console.log("register_init");
  var result = {};
  var new_id = ObjectID();

  async.waterfall([
    function(callback) {
      if (!req.body.email || req.body.email == '') {
        callback({"http_code":"403","message":"email required"},{});
      } else {
        callback(null,{});
      }
    },
    function(data,callback) {
      var filter = {'email':req.body.email};
      req.db.collection('persons').find(filter).toArray(callback);
    },
    function(data,callback) {
      if (data && data.length > 0) {
        callback({"http_code":"403","message":"email exists"},{});
      } else {
        callback(null,{});
      }
    },
    function(data,callback) {
        req.body.create_date = new Date();
        req.body.update_date = new Date();
        req.body.password = '1234';
        req.body._id = new_id;
        req.body.role_merchant = true;
        req.db.collection('persons').insert(req.body,callback);
    },
    function(data,callback) {
      result.id = data.new_id;
      callback(null,{});
    }
  ],
  function(err,data) {
      utils.gen_result(res,err,200,result);
  });
};





// =======================================================================================
                              // register
// =======================================================================================

exports.register = function(req, res){
  console.log("register");

  var id = req.body.id;
  var query = {'_id':ObjectID(id)};
  var options = {"upsert":false, "multi":false};

  var o = req.body;
  // delete o._id;
  var u = {$set:o};

  var resultdata = {};
  async.waterfall([
        function(callback) {
          req.db.collection('').update(query,u,options,function(err){
            callback(err,{});
          });
        },
      ],
      function(err,data) {
        utils.gen_result(res,err,200,result);
      });
};





// =======================================================================================
                              // forgot_password
// =======================================================================================


exports.forgot_password = function(req, res){
  console.log("forgot_password");
  var result = {};

  async.waterfall([
        function(callback) {

            req.db.collection('').insert(o,callback);
        },
        function(data,callback) {
          console.log(JSON.stringify(data));
          result._id = data._id;
          callback(null,{});
        }
      ],
      function(err,data) {
        utils.gen_result(res,err,200,result);
      });
};




// =======================================================================================
                              // profile_getone
// =======================================================================================

exports.profile_getone = function(req, res){
  console.log("profile_getone");
  var id = req.params.id;

  var result = {};

  async.waterfall([
    function(callback) {
      var query = {'_id':ObjectID(id)};
      req.db.collection('').find(query).toArray(callback);
    },
    function(data,callback) {
      if (data.length < 1) {
        callback({"http_code":"404","message":"Profile not found", "dump":{}});
        return;
      }
      result = data[0];
      callback(null,{});
    }
  ],
  function(err,data) {
    utils.gen_result(res,err,200,result);
  });
};





// =======================================================================================
                              // merchant_application
// =======================================================================================

exports.merchant_application = function(req, res){
  console.log("merchant_application");

  var id = req.body.id;
  var query = {'_id':ObjectID(id)};
  var options = {"upsert":false, "multi":false};

  var o = req.body;
  // delete o._id;
  var u = {$set:o};

  var resultdata = {};
  async.waterfall([
        function(callback) {
          req.db.collection('').update(query,u,options,function(err){
            callback(err,{});
          });
        },
      ],
      function(err,data) {
        utils.gen_result(res,err,200,result);
      });
};






// =======================================================================================
                              // profile_update_one
// =======================================================================================

exports.profile_update_one = function(req, res){
  console.log("profile_update_one");

  var id = req.body.id;
  var query = {'_id':ObjectID(id)};
  var options = {"upsert":false, "multi":false};

  var o = req.body;
  // delete o._id;
  var u = {$set:o};

  var resultdata = {};
  async.waterfall([
        function(callback) {
          req.db.collection('').update(query,u,options,function(err){
            callback(err,{});
          });
        },
      ],
      function(err,data) {
        utils.gen_result(res,err,200,result);
      });
};
