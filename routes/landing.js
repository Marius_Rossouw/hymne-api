var async = require('async');
var _ = require('underscore');
var ObjectID = require('mongodb').ObjectID;
var http = require('http');
var querystring = require("querystring");
var moment = require("moment");

var utils = require("../includes/utils.js");


// =======================================================================================
                              // auction_list_landing
// =======================================================================================

exports.auction_list_landing = function(req, res){
  console.log("auction_list_landing");
  var result = {};

  async.waterfall([
        function(callback) {

            req.db.collection('').find().sort({}).toArray(callback);
        },
        function(data,callback) {
          console.log(JSON.stringify(data));
          callback(null,{});
        }
      ],
      function(err,data) {
        utils.gen_result(res,err,200,result);
      });
};




// =======================================================================================
                              // product_list_landing_auctions
// =======================================================================================

exports.product_list_landing_auctions = function(req, res){
  console.log("product_list_landing_auctions");
  var result = {};

  async.waterfall([
        function(callback) {

            req.db.collection('').find().sort({}).toArray(callback);
        },
        function(data,callback) {
          console.log(JSON.stringify(data));
          callback(null,{});
        }
      ],
      function(err,data) {

        if (err) {
          if (err.http_code) {
            res.send(err.http_code,err);
          } else {
            var api_err = {"http_code":"500","message":"system error", "dump":err};
            res.send(500,api_err);
          }
        } else {
          res.send(200,result);
        }
      });
};




// =======================================================================================
                              // product_list_landing_shop
// =======================================================================================

exports.product_list_landing_shopxx = function(req, res){
  console.log("product_list_landing_shopxx");
  var result = {};

  async.waterfall([
        function(callback) {

            req.db.collection('').find().sort({}).toArray(callback);
        },
        function(data,callback) {
          console.log(JSON.stringify(data));
          callback(null,{});
        }
      ],
      function(err,data) {

        if (err) {
          if (err.http_code) {
            res.send(err.http_code,err);
          } else {
            var api_err = {"http_code":"500","message":"system error", "dump":err};
            res.send(500,api_err);
          }
        } else {
          res.send(200,result);
        }
      });
};



// =======================================================================================
                              // product_landing_shop_list
// =======================================================================================

function isInt(value) {
  return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
}

exports.product_landing_shop_list = function(req, res){
  console.log("product_landing_shop_list");
  var result = {
    records :{
      offset:0,
      limit:20,
      available:0,
      data:[]
    }
  };
  // var query = {'owner_id':req.params.id};

  //if(req.query.offset) {
  if(req.body.offset) {
    offsetstr = req.body.offset;
    if(isInt(offsetstr)){
      result.records.offset = parseInt(offsetstr,10);
    }
  };
  //if(req.query.limit) {
  if(req.body.limit) {
    limitstr = req.body.limit;
    if(isInt(limitstr)){
      result.records.limit = parseInt(limitstr,10);
    }
  };

  async.waterfall([
    function(callback) {
        // req.db.collection('products').find(query).sort({}).toArray(callback);

      req.db.collection('products').find({deleted:{$ne:true}}).count(callback);
    },
    function(data,callback) {
      result.records.available = data;
      req.db.collection('products').find({deleted:{$ne:true}}).sort({title:1}).skip(result.records.offset).limit(result.records.limit).toArray(callback);
    },
    function(data,callback) {
      for(var i=0; data.length > i; i++){
        for(var j=0; data[i].images.length > j; j++){
          if (data[i].images[j].main === true){
            data[i].main_pic = data[i].images[j].file_name;
          }
        }
      }
      result.records.data = data;
      callback(null,{});
    }
  ],
  function(err,data) {
    utils.gen_result(res,err,200,result);
  });
};





// =======================================================================================
                              // product_list_landing_shop
// =======================================================================================

function isInt(value) {
  return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
}

exports.product_list_landing_shop = function(req, res){
  console.log("product_list_landing_shop");
  var result = {
    records :{
      offset:0,
      limit:20,
      available:0,
      data:[]
    }
  };
  // var query = {'owner_id':req.params.id};

  //if(req.query.offset) {
  if(req.body.offset) {
    offsetstr = req.body.offset;
    if(isInt(offsetstr)){
      result.records.offset = parseInt(offsetstr,10);
    }
  };
  //if(req.query.limit) {
  if(req.body.limit) {
    limitstr = req.body.limit;
    if(isInt(limitstr)){
      result.records.limit = parseInt(limitstr,10);
    }
  };

  if(!req.body.collection_id){
    req.body.collection_id = '';
  };
  if(req.body.collection_id == 's4') {
    req.body.collection_id = '';
  }
  var filter = {deleted:{$ne:true}};
      //filter.legacy_code = 90;
      filter.collection_id = req.body.collection_id;

  async.waterfall([
    function(callback) {
        // req.db.collection('products').find(query).sort({}).toArray(callback);

      req.db.collection('products').find(filter).count(callback);
    },
    function(data,callback) {
      result.records.available = data;
      console.log("Filter:");
      console.log(JSON.stringify(filter));
      req.db.collection('products').find(filter).sort({capture_time:-1, title:1}).skip(result.records.offset).limit(result.records.limit).toArray(callback);
    },
    function(data,callback) {
      for(var i=0; data.length > i; i++){
        for(var j=0; data[i].images.length > j; j++){
          if (data[i].images[j].main === true){
            data[i].main_pic = data[i].images[j].file_name;
          }
        }
      }
      result.records.data = data;
      callback(null,{});
    }
  ],
  function(err,data) {
    utils.gen_result(res,err,200,result);
  });
};



// =======================================================================================
                              // product_list_landing_shop_filtered
// =======================================================================================

exports.product_list_landing_shop_filtered = function(req, res){
  console.log("product_list_landing_shop_filtered");
  console.log(JSON.stringify(req.body));

  var result = {
    records :{
      offset:0,
      limit:20,
      available:0,
      data:[]
    }
  };
  // var query = {'owner_id':req.params.id};

  //if(req.query.offset) {
  if(req.body.offset) {
    offsetstr = req.body.offset;
    if(isInt(offsetstr)){
      result.records.offset = parseInt(offsetstr,10);
    }
  };
  //if(req.query.limit) {
  if(req.body.limit) {
    limitstr = req.body.limit;
    if(isInt(limitstr)){
      result.records.limit = parseInt(limitstr,10);
    }
  };

  var ands = []
  var filter = {'$and':ands};
  if(req.body.museum && req.body.museum != ''){
    ands.push({'$or':[{mineral_size_type:req.body.museum}]});
  }
  if(req.body.cabinet && req.body.cabinet != ''){
    ands.push({'$or':[{mineral_size_type:req.body.cabinet}]});
  }
  if(req.body.wholesale && req.body.wholesale != ''){
    ands.push({'$or':[{mineral_size_type:req.body.wholesale}]});
  }
  if(req.body.thumbnail && req.body.thumbnail != ''){
    ands.push({'$or':[{mineral_size_type:req.body.thumbnail}]});
  }
  if(req.body.small_cabinet && req.body.small_cabinet != ''){
    ands.push({'$or':[{mineral_size_type:req.body.small_cabinet}]});
  }
  if(req.body.miniature && req.body.miniature != ''){
    ands.push({'$or':[{mineral_size_type:req.body.miniature}]});
  }

  if(req.body.chwaning_wessels_hatazel && req.body.chwaning_wessels_hatazel != ''){
    ands.push({'$or':[{categories:req.body.chwaning_wessels_hatazel}]});
  }
  if(req.body.rest_sa && req.body.rest_sa != ''){
    ands.push({'$or':[{categories:req.body.rest_sa}]});
  }
  if(req.body.brandberg_erongo_okorusu && req.body.brandberg_erongo_okorusu != ''){
    ands.push({'$or':[{categories:req.body.brandberg_erongo_okorusu}]});
  }
  if(req.body.world_selection && req.body.world_selection != ''){
    ands.push({'$or':[{categories:req.body.world_selection}]});
  }
  if(req.body.sa_and_world_mineral_books && req.body.sa_and_world_mineral_books != ''){
    ands.push({'$or':[{categories:req.body.sa_and_world_mineral_books}]});
  }
  if(req.body.okandawasi_koakoland && req.body.okandawasi_koakoland != ''){
    ands.push({'$or':[{categories:req.body.okandawasi_koakoland}]});
  }
  if(req.body.rosh_pinah_skorpion && req.body.rosh_pinah_skorpion != ''){
    ands.push({'$or':[{categories:req.body.rosh_pinah_skorpion}]});
  }
  if(req.body.orange_river_riemvasmaak && req.body.orange_river_riemvasmaak != ''){
    ands.push({'$or':[{categories:req.body.orange_river_riemvasmaak}]});
  }
  if(req.body.wholesale_lots && req.body.wholesale_lots != ''){
    ands.push({'$or':[{categories:req.body.wholesale_lots}]});
  }
  if(req.body.colin_collection && req.body.colin_collection != ''){
    ands.push({'$or':[{categories:req.body.colin_collection}]});
  }
  if(req.body.sugilite_wessels && req.body.sugilite_wessels != ''){
    ands.push({'$or':[{categories:req.body.sugilite_wessels}]});
  }
  if(req.body.tsumeb_kombat_aukas && req.body.tsumeb_kombat_aukas != ''){
    ands.push({'$or':[{categories:req.body.tsumeb_kombat_aukas}]});
  }
  if(req.body.pictures_mines && req.body.pictures_mines != ''){
    ands.push({'$or':[{categories:req.body.pictures_mines}]});
  }
  if(req.body.meteorites && req.body.meteorites != ''){
    ands.push({'$or':[{categories:req.body.meteorites}]});
  }
  if(req.body.musina_zimbabwe_zambia && req.body.musina_zimbabwe_zambia != ''){
    ands.push({'$or':[{categories:req.body.musina_zimbabwe_zambia}]});
  }
  
  
  if(req.body.exeptional && req.body.exeptional != ''){
    ands.push({'$or':[{exeptional_yn:req.body.exeptional}]});
  }
  if(req.body.fluorescent && req.body.fluorescent != ''){
    ands.push({'$or':[{fluorescent_yn:req.body.fluorescent}]});
  }
 

  if(req.body){
    ands.push({'deleted': {$ne:true}});
  }

  console.log(JSON.stringify(filter));

  var result = [];

  async.waterfall([
    function(callback) {
      req.db.collection('products').find(filter).sort({name:1}).toArray(callback);
    },
    function(data,callback) {
      result.records.available = data;
      req.db.collection('products').find({deleted:{$ne:true}}).sort({title:1}).skip(result.records.offset).limit(result.records.limit).toArray(callback);
    },
    function(data,callback) {
      for(var i=0; data.length > i; i++){
        for(var j=0; data[i].images.length > j; j++){
          if (data[i].images[j].main === true){
            data[i].main_pic = data[i].images[j].file_name;
          }
        }
      }
      result.records.data = data;
      callback(null,{});
    }
  ],
  function(err,data) {
    utils.gen_result(res,err,200,result);
  });
};




// =======================================================================================
                              // product_list_lnding_shop_search_string
// =======================================================================================

exports.product_list_lnding_shop_search_string = function(req, res){
  console.log("product_list_lnding_shop_search_string");

  var result = {
    records :{
      offset:0,
      limit:20,
      available:0,
      data:[]
    }
  };
  // var query = {'owner_id':req.params.id};

  //if(req.query.offset) {
  if(req.body.offset) {
    offsetstr = req.body.offset;
    if(isInt(offsetstr)){
      result.records.offset = parseInt(offsetstr,10);
    }
  };
  //if(req.query.limit) {
  if(req.body.limit) {
    limitstr = req.body.limit;
    if(isInt(limitstr)){
      result.records.limit = parseInt(limitstr,10);
    }
  };

  // var resp = {};
  async.waterfall([
    function(callback) {
      
      var filter_and = [];
      filter_and.push({deleted:{$ne:true}});


      if(!req.body.search_string || req.body.search_string.length < 1) {
        console.log('Search String: has no lenth');
      } else {
        console.log('Search String: ' + req.body.search_string);

        filter_and.push({
          $or:[
            {title:{$regex:(req.body.search_string), $options: 'i'}},
            {description_short:{$regex:(req.body.search_string), $options: 'i'}},
          ]
        });
      }

      var filter = {$and:filter_and};
      console.log('Filter: ' + JSON.stringify(filter));
      var sort = {};
      var sort_by = req.body.sort_by;
      console.log('Sort By: ' + req.body.sort_by);

      if(sort_by === 'Higher Price') {
        sort = {price:-1};
      }
      if(sort_by === 'Lower Price') {
        sort = {price:1};
      }
      if(sort_by === 'Alphabetically') {
        sort = {name:1};
      }
      console.log('Sort : ' + sort);
      req.db.collection('products').find(filter).sort(sort).toArray(callback);
    },
    function(data,callback) {
      result.records.available = data;
      req.db.collection('products').find().sort().skip(result.records.offset).limit(result.records.limit).toArray(callback);
    },
    function(data,callback) {
      for(var i=0; data.length > i; i++){
        for(var j=0; data[i].images.length > j; j++){
          if (data[i].images[j].main === true){
            data[i].main_pic = data[i].images[j].file_name;
          }
        }
      }
      result.records.data = data;
      callback(null,{});
    }
  ],
  function(err,data) {
    if (err) {
      var api_err = {"http_code":"500","message":"system error", "dump":err};
      res.send(500,api_err);
    } else {
      var newdata = [];
      newdata = data;
      console.log(JSON.stringify(newdata));
      res.send(200,newdata);
    }
  });
};





// =======================================================================================
                              // product_list_landing_auction
// =======================================================================================

// function isInt(value) {
//   return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
// }

exports.product_list_landing_auction = function(req, res){
  console.log("product_list_landing_auction");
  var result = {
    records :{
      offset:0,
      limit:20,
      available:0,
      data:[]
    }
  };
  // var query = {'owner_id':req.params.id};

  //if(req.query.offset) {
  if(req.body.offset) {
    offsetstr = req.body.offset;
    if(isInt(offsetstr)){
      result.records.offset = parseInt(offsetstr,10);
    }
  };
  //if(req.query.limit) {
  if(req.body.limit) {
    limitstr = req.body.limit;
    if(isInt(limitstr)){
      result.records.limit = parseInt(limitstr,10);
    }
  };
  var filter = {deleted:{$ne:true}};
      filter.type_sale = 'Part of Auction';

  async.waterfall([
    function(callback) {
        // req.db.collection('products').find(query).sort({}).toArray(callback);

      req.db.collection('products').find(filter).count(callback);
    },
    function(data,callback) {
      result.records.available = data;
      req.db.collection('products').find(filter).sort({capture_time:-1, title:1}).skip(result.records.offset).limit(result.records.limit).toArray(callback);
    },
    function(data,callback) {
      for(var i=0; data.length > i; i++){
        for(var j=0; data[i].images.length > j; j++){
          if (data[i].images[j].main === true){
            data[i].main_pic = data[i].images[j].file_name;
          }
        }
      }
      result.records.data = data;
      callback(null,{});
    }
  ],
  function(err,data) {
    utils.gen_result(res,err,200,result);
  });
};



// =======================================================================================
                              // product_list_landing_auction_filtered
// =======================================================================================

exports.product_list_landing_auction_filtered = function(req, res){
  console.log("product_list_landing_auction_filtered");
  console.log(JSON.stringify(req.body));

  var result = {
    records :{
      offset:0,
      limit:20,
      available:0,
      data:[]
    }
  };
  // var query = {'owner_id':req.params.id};

  //if(req.query.offset) {
  if(req.body.offset) {
    offsetstr = req.body.offset;
    if(isInt(offsetstr)){
      result.records.offset = parseInt(offsetstr,10);
    }
  };
  //if(req.query.limit) {
  if(req.body.limit) {
    limitstr = req.body.limit;
    if(isInt(limitstr)){
      result.records.limit = parseInt(limitstr,10);
    }
  };

  var ands = []
  var filter = {'$and':ands};
  if(req.body.museum && req.body.museum != ''){
    ands.push({'$or':[{mineral_size_type:req.body.museum}]});
  }
  if(req.body.cabinet && req.body.cabinet != ''){
    ands.push({'$or':[{mineral_size_type:req.body.cabinet}]});
  }
  if(req.body.wholesale && req.body.wholesale != ''){
    ands.push({'$or':[{mineral_size_type:req.body.wholesale}]});
  }
  if(req.body.thumbnail && req.body.thumbnail != ''){
    ands.push({'$or':[{mineral_size_type:req.body.thumbnail}]});
  }
  if(req.body.small_cabinet && req.body.small_cabinet != ''){
    ands.push({'$or':[{mineral_size_type:req.body.small_cabinet}]});
  }
  if(req.body.miniature && req.body.miniature != ''){
    ands.push({'$or':[{mineral_size_type:req.body.miniature}]});
  }

  if(req.body.chwaning_wessels_hatazel && req.body.chwaning_wessels_hatazel != ''){
    ands.push({'$or':[{categories:req.body.chwaning_wessels_hatazel}]});
  }
  if(req.body.rest_sa && req.body.rest_sa != ''){
    ands.push({'$or':[{categories:req.body.rest_sa}]});
  }
  if(req.body.brandberg_erongo_okorusu && req.body.brandberg_erongo_okorusu != ''){
    ands.push({'$or':[{categories:req.body.brandberg_erongo_okorusu}]});
  }
  if(req.body.world_selection && req.body.world_selection != ''){
    ands.push({'$or':[{categories:req.body.world_selection}]});
  }
  if(req.body.sa_and_world_mineral_books && req.body.sa_and_world_mineral_books != ''){
    ands.push({'$or':[{categories:req.body.sa_and_world_mineral_books}]});
  }
  if(req.body.okandawasi_koakoland && req.body.okandawasi_koakoland != ''){
    ands.push({'$or':[{categories:req.body.okandawasi_koakoland}]});
  }
  if(req.body.rosh_pinah_skorpion && req.body.rosh_pinah_skorpion != ''){
    ands.push({'$or':[{categories:req.body.rosh_pinah_skorpion}]});
  }
  if(req.body.orange_river_riemvasmaak && req.body.orange_river_riemvasmaak != ''){
    ands.push({'$or':[{categories:req.body.orange_river_riemvasmaak}]});
  }
  if(req.body.wholesale_lots && req.body.wholesale_lots != ''){
    ands.push({'$or':[{categories:req.body.wholesale_lots}]});
  }
  if(req.body.colin_collection && req.body.colin_collection != ''){
    ands.push({'$or':[{categories:req.body.colin_collection}]});
  }
  if(req.body.sugilite_wessels && req.body.sugilite_wessels != ''){
    ands.push({'$or':[{categories:req.body.sugilite_wessels}]});
  }
  if(req.body.tsumeb_kombat_aukas && req.body.tsumeb_kombat_aukas != ''){
    ands.push({'$or':[{categories:req.body.tsumeb_kombat_aukas}]});
  }
  if(req.body.pictures_mines && req.body.pictures_mines != ''){
    ands.push({'$or':[{categories:req.body.pictures_mines}]});
  }
  if(req.body.meteorites && req.body.meteorites != ''){
    ands.push({'$or':[{categories:req.body.meteorites}]});
  }
  if(req.body.musina_zimbabwe_zambia && req.body.musina_zimbabwe_zambia != ''){
    ands.push({'$or':[{categories:req.body.musina_zimbabwe_zambia}]});
  }
  
  
  if(req.body.exeptional && req.body.exeptional != ''){
    ands.push({'$or':[{exeptional_yn:req.body.exeptional}]});
  }
  if(req.body.fluorescent && req.body.fluorescent != ''){
    ands.push({'$or':[{fluorescent_yn:req.body.fluorescent}]});
  }
 

  if(req.body){
    ands.push({'deleted': {$ne:true}});
  }

  console.log(JSON.stringify(filter));

  var result = [];

  async.waterfall([
    function(callback) {
      req.db.collection('products').find(filter).sort({name:1}).toArray(callback);
    },
    function(data,callback) {
      result.records.available = data;
      req.db.collection('products').find({deleted:{$ne:true}}).sort({title:1}).skip(result.records.offset).limit(result.records.limit).toArray(callback);
    },
    function(data,callback) {
      for(var i=0; data.length > i; i++){
        for(var j=0; data[i].images.length > j; j++){
          if (data[i].images[j].main === true){
            data[i].main_pic = data[i].images[j].file_name;
          }
        }
      }
      result.records.data = data;
      callback(null,{});
    }
  ],
  function(err,data) {
    utils.gen_result(res,err,200,result);
  });
};




// =======================================================================================
                              // product_list_lnding_auction_search_string
// =======================================================================================

exports.product_list_lnding_auction_search_string = function(req, res){
  console.log("product_list_lnding_auction_search_string");

  var result = {
    records :{
      offset:0,
      limit:20,
      available:0,
      data:[]
    }
  };
  // var query = {'owner_id':req.params.id};

  //if(req.query.offset) {
  if(req.body.offset) {
    offsetstr = req.body.offset;
    if(isInt(offsetstr)){
      result.records.offset = parseInt(offsetstr,10);
    }
  };
  //if(req.query.limit) {
  if(req.body.limit) {
    limitstr = req.body.limit;
    if(isInt(limitstr)){
      result.records.limit = parseInt(limitstr,10);
    }
  };

  // var resp = {};
  async.waterfall([
    function(callback) {
      
      var filter_and = [];
      filter_and.push({deleted:{$ne:true}});


      if(!req.body.search_string || req.body.search_string.length < 1) {
        console.log('Search String: has no lenth');
      } else {
        console.log('Search String: ' + req.body.search_string);

        filter_and.push({
          $or:[
            {title:{$regex:(req.body.search_string), $options: 'i'}},
            {description_short:{$regex:(req.body.search_string), $options: 'i'}},
          ]
        });
      }

      var filter = {$and:filter_and};
      console.log('Filter: ' + JSON.stringify(filter));
      var sort = {};
      var sort_by = req.body.sort_by;
      console.log('Sort By: ' + req.body.sort_by);

      if(sort_by === 'Higher Price') {
        sort = {price:-1};
      }
      if(sort_by === 'Lower Price') {
        sort = {price:1};
      }
      if(sort_by === 'Alphabetically') {
        sort = {name:1};
      }
      console.log('Sort : ' + sort);
      req.db.collection('products').find(filter).sort(sort).toArray(callback);
    },
    function(data,callback) {
      result.records.available = data;
      req.db.collection('products').find().sort().skip(result.records.offset).limit(result.records.limit).toArray(callback);
    },
    function(data,callback) {
      for(var i=0; data.length > i; i++){
        for(var j=0; data[i].images.length > j; j++){
          if (data[i].images[j].main === true){
            data[i].main_pic = data[i].images[j].file_name;
          }
        }
      }
      result.records.data = data;
      callback(null,{});
    }
  ],
  function(err,data) {
    if (err) {
      var api_err = {"http_code":"500","message":"system error", "dump":err};
      res.send(500,api_err);
    } else {
      var newdata = [];
      newdata = data;
      console.log(JSON.stringify(newdata));
      res.send(200,newdata);
    }
  });
};