var async = require('async');
var _ = require('underscore');
var ObjectID = require('mongodb').ObjectID;
var http = require('http');
var querystring = require("querystring");
var moment = require("moment");

var utils = require("../includes/utils.js");



// =======================================================================================
                              // product_getone
// =======================================================================================

exports.product_getone = function(req, res){
  console.log("product_getone");

  var result = {};

  async.waterfall([
    function(callback) {
      var query = {'_id':ObjectID(req.params.id)};
      // req.db.collection('products').findOne(query,callback);
      req.db.collection('products').find(query).toArray(callback);
    },
    function(data,callback) {
      if (data.length < 1) {
        callback({"http_code":"404","message":"Item not found", "dump":{}});
        return;
      }
      result = data[0];
      for(var j=0; result.images.length > j; j++){
        if (result.images[j].main === true){
          result.main_pic = result.images[j].file_name;
        }
      }
      callback(null,{});
    }//,
    //function(data,callback){
    //  add_mercant_details(result,function(err){
    //    callback(err,{});
    //  });
    //}


    // },
    // function(data,callback) {
    //   if (!data) {
    //     callback({"http_code":"404","message":"Product Not Found"});
    //     return;
    //   }
      // data.id = data._id;
      // delete data._id;
      // result = data;

    //   callback(null,{});
    // }
  ],
  function(err,data) {
    utils.gen_result(res,err,200,result);
  });



function add_mercant_details(item,callback){
    if (!item.merchant_id){
      callback(null);
    } else {

      var filter = {deleted:{$ne:true}, _id:item.merchant_id};

      req.db.collection('persons').findOne(filter,function(err,data){
        if (data){
          item.merchant_name = utils.concat_names([data.firstname,data.lastname]);
          item.merchant_avatar = data.avatar;
          item.merchant_email = data.email;
          item.merchant_physical_country = data.physical_country;
          item.merchant_physical_city = data.physical_city;
          item.merchant_description = data.description;
        }
        callback(null);
      });
    };
  }
};





// =======================================================================================
                              // bid_history_product
// =======================================================================================

exports.bid_history_product = function(req, res){
  console.log("bid_history_product");
  var result = {};

  async.waterfall([
        function(callback) {

            req.db.collection('products').find().sort({}).toArray(callback);
        },
        function(data,callback) {
          console.log(JSON.stringify(data));
          callback(null,{});
        }
      ],
      function(err,data) {
        utils.gen_result(res,err,200,result);
      });
};




// =======================================================================================
                              // product_list_merchant
// =======================================================================================

function isInt(value) {
  return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
}

exports.product_list_merchant = function(req, res){
  console.log("product_list_merchant");
  var result = {
    records :{
      offset:0,
      limit:20,
      available:0,
      data:[]
    }
  };
  // var query = {'owner_id':req.params.id};

  //if(req.query.offset) {
  if(req.body.offset) {
    offsetstr = req.body.offset;
    if(isInt(offsetstr)){
      result.records.offset = parseInt(offsetstr,10);
    }
  };
  //if(req.query.limit) {
  if(req.body.limit) {
    limitstr = req.body.limit;
    if(isInt(limitstr)){
      result.records.limit = parseInt(limitstr,10);
    }
  };

  if(!req.body.collection_id){
    req.body.offset = '';
  }

  async.waterfall([
    function(callback) {
        // req.db.collection('products').find(query).sort({}).toArray(callback);

      req.db.collection('products').find({deleted:{$ne:true}, collection_id: req.body.collection_id }).count(callback);
    },
    function(data,callback) {
      result.records.available = data;
      req.db.collection('products').find({deleted:{$ne:true}, collection_id: req.body.collection_id}).sort({capture_time:-1, title:1}).skip(result.records.offset).limit(result.records.limit).toArray(callback);
    },
    function(data,callback) {
      for(var i=0; data.length > i; i++){
        for(var j=0; data[i].images.length > j; j++){
          if (data[i].images[j].main === true){
            data[i].main_pic = data[i].images[j].file_name;
          }
        }
      }
      result.records.data = data;
      callback(null,{});
    }
  ],
  function(err,data) {
    utils.gen_result(res,err,200,result);
  });
};


// =======================================================================================
                              // views_add_one
// =======================================================================================

exports.views_add_one = function(req, res){
  console.log("views_add_one");
  var result = {};
  consle.log('req.body.item_id' + req.body.item_id);
  consle.log('req.body.person_id' + req.body.person_id);
  var item_id = ObjectID(req.body.item_id);
  req.body.item_id = item_id;
  var person_id = ObjectID(req.body.person_id);
  req.body.person_id = person_id;


  var resultdata = {};
  async.waterfall([
    function(callback) {

        req.db.collection('views').insert(req.body,callback);
    },
    function(data,callback) {
      result = data;
      callback(null,{});
    }
  ],
  function(err,data) {
    utils.gen_result(res,err,200,result);
  });
};


// =======================================================================================
                              // views_return
// =======================================================================================


exports.views_return = function(req, res){
  console.log("views_return");

  var result = {};
  var item_id = ObjectID(req.body.item_id);
  req.body.item_id = item_id;
  var person_id = ObjectID(req.body.person_id);
  req.body.person_id = person_id;

  async.waterfall([
    function(callback) {
      var query = {'item_id':req.body.item_id};
      // req.db.collection('products').findOne(query,callback);
      req.db.collection('views').find(query).toArray(callback);
    },
    function(data,callback) {
      if (data.length < 1) {
        callback({"http_code":"404","message":"Item not found", "dump":{}});
        return;
      }
      result = data.length;
      callback(null,{});
    }
  ],
  function(err,data) {
    utils.gen_result(res,err,200,result);
  });
};
// =======================================================================================
                              // product_update
// =======================================================================================

exports.product_update = function(req, res){
  console.log("product_update");
  var result = {};
  var id = ObjectID();

  var insert_yn = true;
  if(req.body._id && req.body._id.length > 0 && req.body._id != 'new') {
    insert_yn = false;
  }

  var resultdata = {};
  async.waterfall([
    function(callback) {
      if (insert_yn && (req.body.type == 'Minerals')) {
        var query = { name:'product_no' };
        var sort = [];
        var update = {$inc:{seq:1}};
        var options = {new: true};
        req.db.collection('counters').findAndModify(query,sort,update,options,callback);
      } else {
        callback(null,{});
      };
    },
    function(data,callback) {
      if (insert_yn) {
        console.log(data);
        req.body.product_no = data.value.seq;
        req.body._id = id;
        req.body.capture_date = moment().format("YYYY-MM-DD");
        req.body.capture_time = moment().toDate();
        req.db.collection('products').insert(req.body,callback);
      } else {
        var query = {'_id':ObjectID(req.body._id)};
        var options = {"upsert":false, "multi":false};
        delete req.body._id;
        delete req.body.capture_date;
        delete req.body.capture_time;
        req.body.update_date = moment().format("YYYY-MM-DD");
        req.body.update_time = moment().toDate();
        req.db.collection('products').update(query,{$set:req.body},options,callback);
      }
    },
    function(data,callback) {
      result = {_id:id};
      callback(null,{});
    }
  ],
  function(err,data) {
    utils.gen_result(res,err,200,result);
  });
};




// =======================================================================================
                              // product_update_winner
// =======================================================================================

exports.product_update_winner = function(req, res){
  console.log("product_update_winner");

  var id = req.body.id;
  var query = {'_id':ObjectID(id)};
  var options = {"upsert":false, "multi":false};

  var o = req.body;
  // delete o._id;
  var u = {$set:o};

  var resultdata = {};
  async.waterfall([
        function(callback) {
          req.db.collection('').update(query,u,options,function(err){
            callback(err,{});
          });
        },
      ],
      function(err,data) {
        utils.gen_result(res,err,200,result);
      });
};




// =======================================================================================
                              // product_shipping
// =======================================================================================

exports.product_shipping = function(req, res){
  console.log("product_shipping");
  var result = {};

  async.waterfall([
        function(callback) {

            req.body.create_date = new Date();
            req.body.update_date = new Date();

            var o = req.body;
            
            req.db.collection('').insert(o,callback);
        },
        function(data,callback) {
          console.log(JSON.stringify(data));
          result._id = data._id;
          callback(null,{});
        }
      ],
      function(err,data) {
        utils.gen_result(res,err,200,result);
      });
};


// =======================================================================================
                              // product_list_filtered
// =======================================================================================

exports.product_list_filtered = function(req, res){
  console.log("product_list_filtered");
  console.log(JSON.stringify(req.body));

  var result = {
    records :{
      offset:0,
      limit:20,
      available:0,
      data:[]
    }
  };
  // var query = {'owner_id':req.params.id};

  //if(req.query.offset) {
  if(req.body.offset) {
    offsetstr = req.body.offset;
    if(isInt(offsetstr)){
      result.records.offset = parseInt(offsetstr,10);
    }
  };
  //if(req.query.limit) {
  if(req.body.limit) {
    limitstr = req.body.limit;
    if(isInt(limitstr)){
      result.records.limit = parseInt(limitstr,10);
    }
  };

  var ands = []
  var filter = {'$and':ands};
  if(req.body.museum && req.body.museum != ''){
    ands.push({'$or':[{mineral_size_type:req.body.museum}]});
  }
  if(req.body.cabinet && req.body.cabinet != ''){
    ands.push({'$or':[{mineral_size_type:req.body.cabinet}]});
  }
  if(req.body.wholesale && req.body.wholesale != ''){
    ands.push({'$or':[{mineral_size_type:req.body.wholesale}]});
  }
  if(req.body.thumbnail && req.body.thumbnail != ''){
    ands.push({'$or':[{mineral_size_type:req.body.thumbnail}]});
  }
  if(req.body.small_cabinet && req.body.small_cabinet != ''){
    ands.push({'$or':[{mineral_size_type:req.body.small_cabinet}]});
  }
  if(req.body.miniature && req.body.miniature != ''){
    ands.push({'$or':[{mineral_size_type:req.body.miniature}]});
  }

  if(req.body.chwaning_wessels_hatazel && req.body.chwaning_wessels_hatazel != ''){
    ands.push({'$or':[{categories:req.body.chwaning_wessels_hatazel}]});
  }
  if(req.body.rest_sa && req.body.rest_sa != ''){
    ands.push({'$or':[{categories:req.body.rest_sa}]});
  }
  if(req.body.brandberg_erongo_okorusu && req.body.brandberg_erongo_okorusu != ''){
    ands.push({'$or':[{categories:req.body.brandberg_erongo_okorusu}]});
  }
  if(req.body.world_selection && req.body.world_selection != ''){
    ands.push({'$or':[{categories:req.body.world_selection}]});
  }
  if(req.body.sa_and_world_mineral_books && req.body.sa_and_world_mineral_books != ''){
    ands.push({'$or':[{categories:req.body.sa_and_world_mineral_books}]});
  }
  if(req.body.okandawasi_koakoland && req.body.okandawasi_koakoland != ''){
    ands.push({'$or':[{categories:req.body.okandawasi_koakoland}]});
  }
  if(req.body.rosh_pinah_skorpion && req.body.rosh_pinah_skorpion != ''){
    ands.push({'$or':[{categories:req.body.rosh_pinah_skorpion}]});
  }
  if(req.body.orange_river_riemvasmaak && req.body.orange_river_riemvasmaak != ''){
    ands.push({'$or':[{categories:req.body.orange_river_riemvasmaak}]});
  }
  if(req.body.wholesale_lots && req.body.wholesale_lots != ''){
    ands.push({'$or':[{categories:req.body.wholesale_lots}]});
  }
  if(req.body.colin_collection && req.body.colin_collection != ''){
    ands.push({'$or':[{categories:req.body.colin_collection}]});
  }
  if(req.body.sugilite_wessels && req.body.sugilite_wessels != ''){
    ands.push({'$or':[{categories:req.body.sugilite_wessels}]});
  }
  if(req.body.tsumeb_kombat_aukas && req.body.tsumeb_kombat_aukas != ''){
    ands.push({'$or':[{categories:req.body.tsumeb_kombat_aukas}]});
  }
  if(req.body.pictures_mines && req.body.pictures_mines != ''){
    ands.push({'$or':[{categories:req.body.pictures_mines}]});
  }
  if(req.body.meteorites && req.body.meteorites != ''){
    ands.push({'$or':[{categories:req.body.meteorites}]});
  }
  if(req.body.musina_zimbabwe_zambia && req.body.musina_zimbabwe_zambia != ''){
    ands.push({'$or':[{categories:req.body.musina_zimbabwe_zambia}]});
  }
  if(req.body.exeptional && req.body.exeptional != ''){
    ands.push({'$or':[{exeptional_yn:req.body.exeptional}]});
  }
  if(req.body.fluorescent && req.body.fluorescent != ''){
    ands.push({'$or':[{fluorescent_yn:req.body.fluorescent}]});
  }
  if(req.body.legacy_code_90 && req.body.legacy_code_90 != ''){
    ands.push({'$or':[{legacy_code:req.body.legacy_code_90}]});
  }
  if(req.body.legacy_code_95 && req.body.legacy_code_95 != ''){
    ands.push({'$or':[{legacy_code:req.body.legacy_code_95}]});
  }
  if(req.body.legacy_code_50 && req.body.legacy_code_50 != ''){
    ands.push({'$or':[{legacy_code:req.body.legacy_code_50}]});
  }
  if(req.body.legacy_code_0 && req.body.legacy_code_0 != ''){
    ands.push({'$or':[{legacy_code:req.body.legacy_code_0}]});
  }
  if(req.body.special_characteristics_gem && req.body.special_characteristics_gem != ''){
    ands.push({'$or':[{special_characteristics:req.body.special_characteristics_gem}]});
  }
  if(req.body.special_characteristics_discover && req.body.special_characteristics_discover != ''){
    ands.push({'$or':[{special_characteristics:req.body.special_characteristics_discover}]});
  }
  if(req.body.special_characteristics_new_type && req.body.special_characteristics_new_type != ''){
    ands.push({'$or':[{special_characteristics:req.body.special_characteristics_new_type}]});
  }
  if(req.body.special_characteristics_pseudomorph && req.body.special_characteristics_pseudomorph != ''){
    ands.push({'$or':[{special_characteristics:req.body.special_characteristics_pseudomorph}]});
  }
  if(req.body.special_characteristics_radioactive && req.body.special_characteristics_radioactive != ''){
    ands.push({'$or':[{special_characteristics:req.body.special_characteristics_radioactive}]});
  }
  if(req.body.special_characteristics_rare && req.body.special_characteristics_rare != ''){
    ands.push({'$or':[{special_characteristics:req.body.special_characteristics_rare}]});
  }
  if(req.body.special_characteristics_locality && req.body.special_characteristics_locality != ''){
    ands.push({'$or':[{special_characteristics:req.body.special_characteristics_locality}]});
  }
  if(req.body.special_characteristics_antique && req.body.special_characteristics_antique != ''){
    ands.push({'$or':[{special_characteristics:req.body.special_characteristics_antique}]});
  }
  if(req.body.special_characteristics_single && req.body.special_characteristics_single != ''){
    ands.push({'$or':[{special_characteristics:req.body.special_characteristics_single}]});
  }
  if(req.body.special_characteristics_cluster && req.body.special_characteristics_cluster != ''){
    ands.push({'$or':[{special_characteristics:req.body.special_characteristics_cluster}]});
  }
  if(req.body.special_characteristics_on_matrix && req.body.special_characteristics_on_matrix != ''){
    ands.push({'$or':[{special_characteristics:req.body.special_characteristics_on_matrix}]});
  }
  if(req.body.special_characteristics_mssive && req.body.special_characteristics_mssive != ''){
    ands.push({'$or':[{special_characteristics:req.body.special_characteristics_mssive}]});
  }
  if(req.body.special_characteristics_combination && req.body.special_characteristics_combination != ''){
    ands.push({'$or':[{special_characteristics:req.body.special_characteristics_combination}]});
  }
  if(req.body.special_characteristics_mixed && req.body.special_characteristics_mixed != ''){
    ands.push({'$or':[{special_characteristics:req.body.special_characteristics_mixed}]});
  }
  if(req.body.special_characteristics_artefact && req.body.special_characteristics_artefact != ''){
    ands.push({'$or':[{special_characteristics:req.body.special_characteristics_artefact}]});
  }
  if(req.body.part_of_auction && req.body.part_of_auction != ''){
    ands.push({'$or':[{type_sale:req.body.part_of_auction}]});
  }
  if(req.body.shop_items && req.body.shop_items != ''){
    ands.push({'$or':[{type_sale:req.body.shop_items}]});
  }
  if(req.body.exeptional && req.body.exeptional != ''){
    ands.push({'$or':[{type_sale:req.body.exeptional}]});
  }

 

  if(req.body){
    ands.push({'deleted': {$ne:true}});
  }

  console.log(JSON.stringify(filter));

  var result = [];

  async.waterfall([
    function(callback) {
      req.db.collection('products').find(filter).sort({name:1}).toArray(callback);
    },
    function(data,callback) {
      result.records.available = data;
      req.db.collection('products').find({deleted:{$ne:true}}).sort({title:1}).skip(result.records.offset).limit(result.records.limit).toArray(callback);
    },
    function(data,callback) {
      for(var i=0; data.length > i; i++){
        for(var j=0; data[i].images.length > j; j++){
          if (data[i].images[j].main === true){
            data[i].main_pic = data[i].images[j].file_name;
          }
        }
      }
      result.records.data = data;
      callback(null,{});
    }
  ],
  function(err,data) {
    utils.gen_result(res,err,200,result);
  });
};




// =======================================================================================
                              // product_list_search_string
// =======================================================================================

exports.product_list_search_string = function(req, res){
  console.log("product_list_search_string");

  var result = {
    records :{
      offset:0,
      limit:20,
      available:0,
      data:[]
    }
  };
  // var query = {'owner_id':req.params.id};

  //if(req.query.offset) {
  if(req.body.offset) {
    offsetstr = req.body.offset;
    if(isInt(offsetstr)){
      result.records.offset = parseInt(offsetstr,10);
    }
  };
  //if(req.query.limit) {
  if(req.body.limit) {
    limitstr = req.body.limit;
    if(isInt(limitstr)){
      result.records.limit = parseInt(limitstr,10);
    }
  };

  // var resp = {};
  async.waterfall([
    function(callback) {
      
      var filter_and = [];
      filter_and.push({deleted:{$ne:true}});


      if(!req.body.search_string || req.body.search_string.length < 1) {
        console.log('Search String: has no lenth');
      } else {
        console.log('Search String: ' + req.body.search_string);

        filter_and.push({
          $or:[
            {title:{$regex:(req.body.search_string), $options: 'i'}},
            {description_short:{$regex:(req.body.search_string), $options: 'i'}},
          ]
        });
      }

      var filter = {$and:filter_and};
      console.log('Filter: ' + JSON.stringify(filter));
      var sort = {};
      var sort_by = req.body.sort_by;
      console.log('Sort By: ' + req.body.sort_by);

      if(sort_by === 'Higher Price') {
        sort = {price:-1};
      }
      if(sort_by === 'Lower Price') {
        sort = {price:1};
      }
      if(sort_by === 'Alphabetically') {
        sort = {name:1};
      }
      console.log('Sort : ' + sort);
      req.db.collection('products').find(filter).sort(sort).toArray(callback);
    },
    function(data,callback) {
      result.records.available = data;
      req.db.collection('products').find().sort().skip(result.records.offset).limit(result.records.limit).toArray(callback);
    },
    function(data,callback) {
      for(var i=0; data.length > i; i++){
        for(var j=0; data[i].images.length > j; j++){
          if (data[i].images[j].main === true){
            data[i].main_pic = data[i].images[j].file_name;
          }
        }
      }
      result.records.data = data;
      callback(null,{});
    }
  ],
  function(err,data) {
    if (err) {
      var api_err = {"http_code":"500","message":"system error", "dump":err};
      res.send(500,api_err);
    } else {
      var newdata = [];
      newdata = data;
      console.log(JSON.stringify(newdata));
      res.send(200,newdata);
    }
  });
};