var async = require('async');
var _ = require('underscore');
var ObjectID = require('mongodb').ObjectID;
var http = require('http');
var querystring = require("querystring");
var moment = require("moment");

var utils = require("../includes/utils.js");

// =======================================================================================
                              // collection_getone
// =======================================================================================

exports.collection_getone = function(req, res){
  console.log("collection_getone");

//   var result = {};
//   console.log('id: ' + req.body.id);
//   async.waterfall([
//     function(callback) {
//       var query = {'_id':ObjectID(req.body.id)};
//       console.log('query: ' + query);
//       req.db.collection('collections').find(query).toArray(callback);
//     },
//     function(data,callback) {
//       if (data.length < 1) {
//       	console.log('No such collection with id: ' + req.body.id);
//         callback({"http_code":"404","message":"Item not found", "dump":{}});
//         return;
//       }
//       result = data[0];
//       console.log('result: ' + result);
//       // for(var j=0; result.images.length > j; j++){
//       //   if (result.images[j].main === true){
//       //     result.main_pic = result.images[j].file_name;
//       //   }
//       // }
//       callback(null,{});
//     }
//   ],
//   function(err,data) {
//     utils.gen_result(res,err,200,result);
//   });
// };

  console.log(req.body.id);
  var id = req.body.id;

  var resp = {};

  async.waterfall([
    function(callback) {
      // console.log(id);
      var query = {'_id':ObjectID(id)};
      req.db.collection('collections').find(query).toArray(callback);
    },
    function(data,callback) {
      if (data.length < 1) {
        callback({"http_code":"404","message":"collections not found", "dump":{}});
        return;
      }
      resp = data[0];
      callback(null,{});
    }
    ],
    function(err,data) {
      if (err) {
        if (err.http_code) {
          res.send(err.http_code,err);
        } else {
          var api_err = {"http_code":"500","message":"system error", "dump":err};
          res.send(500,api_err);
        }
      } else {
        res.send(200,resp);
      }
    });
};

// =======================================================================================
                              // collection_update
// =======================================================================================

exports.collection_update = function(req, res){
  console.log("collection_update");
  var result = {};
  var id = ObjectID();

  var insert_yn = true;
  if(req.body._id && req.body._id.length > 0 && req.body._id != 'new') {
    insert_yn = false;
  }

  var resultdata = {};
  async.waterfall([
    function(callback) {
      if (insert_yn) {
        req.body._id = id;
        console.log("id" + id);
        req.body.capture_date = moment().format("YYYY-MM-DD");
        req.body.capture_time = moment().toDate();
        req.db.collection('collections').insert(req.body,callback);
      } else {
        var query = {'_id':ObjectID(req.body._id)};
        console.log("query" + JSON.stringify(query));
        console.log("b4 body" + JSON.stringify(req.body));
        var options = {"upsert":false, "multi":false};
        delete req.body._id;
        delete req.body.capture_date;
        delete req.body.capture_time;
        req.body.update_date = moment().format("YYYY-MM-DD");
        req.body.update_time = moment().toDate();
        console.log("Afer body" + JSON.stringify(req.body));
        req.db.collection('collections').update(query,{$set:req.body},options,callback);
      }
    },
    function(data,callback) {
      result = {_id:id};
      callback(null,{});
    }
  ],
  function(err,data) {
    utils.gen_result(res,err,200,result);
  });
};




// =======================================================================================
                              // collection_list
// =======================================================================================

exports.collection_list = function(req, res){ 
  console.log("collection_list");

	var result = {};
	var filter = {};

	if(req.body.landing){
	  	filter = {deleted:{$ne:true},'display_yn':true};
	}
	if(req.body.products){
		filter = {deleted:{$ne:true}};
    }
    if(req.body.grid){
		filter = {};
    }
    async.waterfall([
      function(callback) {
      	req.db.collection('collections').find(filter).sort({component:-1}).toArray(callback);
      },
	  function(data,callback) {
	      result = data;
	      console.log('result: ' + result);
	      callback(null,{});
	    }
    ],
    function(err,data) {
    	utils.gen_result(res,err,200,result);
	});
};
