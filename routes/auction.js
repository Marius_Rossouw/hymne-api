var async = require('async');
var _ = require('underscore');
var ObjectID = require('mongodb').ObjectID;
var http = require('http');
var querystring = require("querystring");
var moment = require("moment");

var utils = require("../includes/utils.js");


// =======================================================================================
                              // auction_list
// =======================================================================================

exports.auction_list = function(req, res){
  console.log("auction_list");
  var result = {};

  async.waterfall([
        function(callback) {

            req.db.collection('').find().sort({}).toArray(callback);
        },
        function(data,callback) {
          console.log(JSON.stringify(data));
          callback(null,{});
        }
      ],
      function(err,data) {
        utils.gen_result(res,err,200,result);
      });
};





// =======================================================================================
                              // auction_getone
// =======================================================================================

exports.auction_getone = function(req, res){
  console.log("auction_getone");
  var id = req.body.id;

  var result = {};

  async.waterfall([
    function(callback) {
      var query = {'_id':ObjectID(id)};
      req.db.collection('').find(query).toArray(callback);
    },
    function(data,callback) {
      if (data.length < 1) {
        callback({"http_code":"404","message":"Auction not found", "dump":{}});
        return;
      }
      result = data[0];
      callback(null,{});
    }
  ],
  function(err,data) {
    utils.gen_result(res,err,200,result);
  });
};






// =======================================================================================
                              // bid
// =======================================================================================

exports.bid = function(req, res){
  console.log("bid");
  var result = {};

  async.waterfall([
        function(callback) {

            req.body.create_date = new Date();
            req.body.update_date = new Date();

            var o = req.body;
            if (o.created_by){
              o.created_by = ObjectID(o.created_by);
            }
            req.db.collection('').insert(o,callback);
        },
        function(data,callback) {
          console.log(JSON.stringify(data));
          result._id = data._id;
          callback(null,{});
        }
      ],
      function(err,data) {
        utils.gen_result(res,err,200,result);
      });
};





// =======================================================================================
                              // bid_history_client
// =======================================================================================

exports.bid_history_client = function(req, res){
  console.log("bid_history_client");

  var id = req.body.id;
  var filter1 = {'_id':ObjectID(id)};
  var result = [];

  async.waterfall([
    function(callback) {
      req.db.collection('').find(filter1).sort({}).toArray(callback);
    },
    function(data,callback) {
      if (!data){
        callback({"http_code":"404","message":"No Bid History", "dump":{}},{});
        return;
      }
      result = data;
      callback(null,{});
    }
  ],
  function(err,data) {
    utils.gen_result(res,err,200,result);
  });

};




// =======================================================================================
                              // auction_list_merchant
// =======================================================================================

exports.auction_list_merchant = function(req, res){
  console.log("auction_list_merchant");

  var id = req.body.id;
  var filter1 = {'_id':ObjectID(id)};
  var result = [];

  async.waterfall([
    function(callback) {
      req.db.collection('').find(filter1).sort({}).toArray(callback);
    },
    function(data,callback) {
      if (!data){
        callback({"http_code":"404","message":"No Auctions Found", "dump":{}},{});
        return;
      }
      result = data;
      callback(null,{});
    }
  ],
  function(err,data) {
    utils.gen_result(res,err,200,result);
  });

};




// =======================================================================================
                              // auction_list_admin
// =======================================================================================

exports.auction_list_admin = function(req, res){
  console.log("auction_list_admin");

  var id = req.body.id;
  var filter1 = {'_id':ObjectID(id)};
  var result = [];

  async.waterfall([
    function(callback) {
      req.db.collection('').find(filter1).sort({}).toArray(callback);
    },
    function(data,callback) {
      if (!data){
        callback({"http_code":"404","message":"No Auctions Found", "dump":{}},{});
        return;
      }
      result = data;
      callback(null,{});
    }
  ],
  function(err,data) {
    utils.gen_result(res,err,200,result);
  });

};






// =======================================================================================
                              // auction_update_merchant
// =======================================================================================

exports.auction_update_merchant = function(req, res){
  console.log("auction_update_merchant");

  var id = req.body.id;
  var query = {'_id':ObjectID(id)};
  var options = {"upsert":false, "multi":false};

  var o = req.body;
  // delete o._id;
  var u = {$set:o};

  var resultdata = {};
  async.waterfall([
        function(callback) {
          req.db.collection('').update(query,u,options,function(err){
            callback(err,{});
          });
        },
      ],
      function(err,data) {
        utils.gen_result(res,err,200,result);
      });
};





// =======================================================================================
                              // acution_update_admin
// =======================================================================================

exports.acution_update_admin = function(req, res){
  console.log("acution_update_admin");

  var id = req.body.id;
  var query = {'_id':ObjectID(id)};
  var options = {"upsert":false, "multi":false};

  var o = req.body;
  // delete o._id;
  var u = {$set:o};

  var resultdata = {};
  async.waterfall([
        function(callback) {
          req.db.collection('').update(query,u,options,function(err){
            callback(err,{});
          });
        },
      ],
      function(err,data) {
        utils.gen_result(res,err,200,result);
      });
};






// =======================================================================================
                              // merchant_update
// =======================================================================================

exports.merchant_update = function(req, res){
  console.log("merchant_update");
  var result = {};

  var id = req.body.id;
  var query = {'_id':ObjectID(id)};
  var options = {"upsert":false, "multi":false};

  var o = req.body;
  // delete o._id;
  var u = {$set:o};

  async.waterfall([
        function(callback) {
          req.db.collection('').update(query,u,options,function(err){
            callback(err,{});
          });
        },
      ],
      function(err,data) {
        utils.gen_result(res,err,200,result);
      });
};