var express = require('express');
var http = require('http');
var path = require('path');
var mongoclient = require('mongodb').MongoClient;
var setup = require('./includes/setup.js');
var favicon = require('serve-favicon');
//var express_json = require('express-json');
var bodyParser  = require('body-parser');
var methodOverride = require('method-override')

//create a mongo connection instance
var mongodbconnection;
mongoclient.connect(setup.mongo_db,function(err,data){
    mongodbconnection = data;
});

var logreq = function(req, res, next) {
    //log stuff of the request here
    next();
};
var passInDB = function(req, res, next) {
    req.db = mongodbconnection;
    next();
};
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', req.headers.origin);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    res.header('Access-Control-Allow-Credentials', true);    
    next();
};
var dontcache = function(req, res, next) {
    res.header("Cache-Control", "no-cache, no-store, must-revalidate");
    res.header("Pragma", "no-cache");
    res.header("Expires", 0);
    next();
};

//create express instance and set it up
var app = express();
app.set('port', setup.server_port);

//middlewarea: http://expressjs.com/guide/using-middleware.html
app.use(favicon(path.join(__dirname,"/favicon.ico")));
//app.use(express.logger('dev'));
// app.use(express_json());
// app.use(express.urlencoded());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(methodOverride());
//app.use(logreq);
app.use(allowCrossDomain);
app.use('/files',express.static(__dirname + '/uploads')); 
app.use('/files100',express.static(__dirname + '/uploads100')); 
app.use(dontcache);
app.use(passInDB);
//app.use(app.router);

// =============== route to functions =====================

function default_message(req, res){
   //res.send(200,"<h3>Bogowu API</h3>");
   res.send(404,"");
}
app.get('/', default_message); 


var admin = require('./routes/admin.js');
app.post('/merchant_list', admin.merchant_list);
app.post('/merchant_update/:id', admin.merchant_update);


var auction = require('./routes/auction.js');
app.post('/auction_list', auction.auction_list);
app.post('/auction_getone/:id', auction.auction_getone);
app.post('/bid', auction.bid);
app.post('/bid_history_client/:id', auction.bid_history_client);
app.post('/auction_list_merchant/:id', auction.auction_list_merchant);
app.post('/auction_update_merchant/:id', auction.auction_update_merchant);
app.post('/auction_list_admin', auction.auction_list_admin);
app.post('/acution_update_admin/:id', auction.acution_update_admin);


var multer  = require('multer');
var mymulter = multer({dest: __dirname + '/uploads/'});
var files = require('./routes/files.js');
app.post('/files', [mymulter, files.file_upload]);
app.post('/files_base64', files.file_upload_base64);
app.get('/persons/:person_id/files', files.person_files_list);


var landing = require('./routes/landing.js');
app.post('/auction_list_landing', landing.auction_list_landing);
app.post('/product_list_landing_auctions', landing.product_list_landing_auctions);
app.post('/product_landing_shop_list', landing.product_landing_shop_list);

app.post('/product_list_landing_shop', landing.product_list_landing_shop);
app.post('/product_list_landing_shop_filtered', landing.product_list_landing_shop_filtered);
app.post('/product_list_lnding_shop_search_string', landing.product_list_lnding_shop_search_string);

app.post('/product_list_landing_auction', landing.product_list_landing_auction);
app.post('/product_list_landing_auction_filtered', landing.product_list_landing_auction_filtered);
app.post('/product_list_lnding_auction_search_string', landing.product_list_lnding_auction_search_string);


var merchant = require('./routes/merchant.js');
app.post('/terms_update/:id', merchant.terms_update);
app.post('/paypal_update/:id', merchant.paypal_update);


var product = require('./routes/product.js');
app.post('/product_getone/:id', product.product_getone);
app.post('/bid_history_product/:id', product.bid_history_product);
app.post('/product_list_merchant/:id', product.product_list_merchant);
// app.post('/product_add/', product.product_add);
app.post('/product_update', product.product_update);
//app.post('/product_update/:id', product.product_update);
app.post('/product_list_search_string/', product.product_list_search_string);
app.post('/product_list_filtered/', product.product_list_filtered);
app.post('/product_update_winner', product.product_update_winner);
app.post('/product_shipping', product.product_shipping);
app.post('/views_add_one', product.views_add_one);
app.post('/views_return', product.views_return);

var collection = require('./routes/collection.js');
app.post('/collection_getone/:id', collection.collection_getone);
app.post('/collection_update', collection.collection_update); 
app.post('/collection_list', collection.collection_list);
// app.post('/bid_history_product/:id', collection.bid_history_product);
// app.post('/product_list_merchant/:id', collection.product_list_merchant);
// app.post('/product_add/', collection.product_add);
// app.post('/product_update/:id', collection.product_update);
// app.post('/product_list_search_string/', collection.product_list_search_string);
// app.post('/product_list_filtered/', collection.product_list_filtered);
// app.post('/product_update_winner', collection.product_update_winner);
// app.post('/product_shipping', collection.product_shipping);
// app.post('/views_add_one', collection.views_add_one);
// app.post('/views_return', collection.views_return);


var profile = require('./routes/profile.js');
app.post('/register_init', profile.register_init);
app.post('/register', profile.register);
app.post('/forgot_password', profile.forgot_password);
app.post('/profile_getone/:id', profile.profile_getone);
app.post('/merchant_application', profile.merchant_application);
app.post('/profile_update_one/:id', profile.profile_update_one);


var purchase = require('./routes/purchase.js');
app.post('/pay_test', purchase.pay_test);
app.post('/purchase_history_list', purchase.purchase_history_list);


var session = require('./routes/session.js');
app.post('/login', session.login);


var shop = require('./routes/shop.js');
app.post('/product_list_shop', shop.product_list_shop);
app.post('/shopping_cart_getone/:id', shop.shopping_cart_getone);


// ============== Start listening ===========================

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

