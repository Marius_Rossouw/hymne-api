exports.concat_names = concat_names;

function concat_names(a){
  var name = '';
  for (var i=0;i< a.length;i++){
    if (a[i] && a[i].length > 0){
      if (name.length > 0){
        name = name + ' ';
      }
      name = name + a[i];
    }
  }
  return name;
}


exports.gen_result = gen_result;
function gen_result(res,err,http_code,result){
  if (err) {
    if (err && err.http_code) {
      res.send(err.http_code,err.message);
    } else {
      console.error(err);
      res.send(500,'system_error');
    }
  } else {
    res.send(http_code,result);
  }
}


